import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AppService } from "./app.service";
import { User } from "./shared/models/user";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public user: User;
  constructor(private router: Router, private appService: AppService) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        let url = val["url"] || null;
        if (url != undefined && url.indexOf('login') >= 0) {
          this.appService.user.isAuthor = false;
        } else {
          this.appService.user.isAuthor = true;
        }
        this.user = this.appService.user;
        console.log('this.user.isAuthor:', this.user.isAuthor);
      }
    });
  }

  ngOnInit() {

    this.user = this.appService.user;
   
  }

}
