import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SystemRoutingModule } from './system-routing.module';
// import { SystemComponent } from './system.component';
import { ShareModule } from "../../shared/modules/share/share.module";
// import { AppChildrenSystemComponent } from './app-children-system/app-children-system.component';

@NgModule({
  imports: [
    CommonModule,
    SystemRoutingModule,
    ShareModule
  ],
  declarations: []
})
export class SystemModule { }
