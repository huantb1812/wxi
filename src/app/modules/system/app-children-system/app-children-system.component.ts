import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-children-system',
  templateUrl: './app-children-system.component.html',
  styleUrls: ['./app-children-system.component.css']
})
export class AppChildrenSystemComponent implements OnInit {
  @Input() detail: any;

  constructor() { }

  ngOnInit() {
  }

}
