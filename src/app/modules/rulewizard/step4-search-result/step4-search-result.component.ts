import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'step4-search-result',
  templateUrl: './step4-search-result.component.html',
  styleUrls: ['./step4-search-result.component.css']
})
export class Step4SearchResultComponent implements OnInit {
  @Input() resultsAllData:any;
  @Input() searchResult: Array<any>;
  public curentDetail: any;
  constructor() {
    this.searchResult = [];
  }

  ngOnInit() {
  }
  changeCheckBox(event, item, index) {
    for (let i = 0; i < this.searchResult.length; i++) {
      let item = this.searchResult[i];
      if (i == index) {
        if (item.selected != true) { this.curentDetail = item; }
        else {
          this.curentDetail = null;
        }
      } else {
        item.selected = false;
      }

    }

    console.log(this.searchResult);
  }
}
