import { Component, OnInit, Input } from '@angular/core';
import { SearchModel } from "../models/wizard";
@Component({
  selector: 'step2-patients-demographics',
  templateUrl: './step2-patients-demographics.component.html',
  styleUrls: ['./step2-patients-demographics.component.css']
})
export class Step2PatientsDemographicsComponent implements OnInit {
  @Input() searchModel: SearchModel;
  constructor() {
    this.searchModel = new SearchModel();
  }

  ngOnInit() {
  }

}
