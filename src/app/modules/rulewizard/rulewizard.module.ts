import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RulewizardRoutingModule } from './rulewizard-routing.module';
// import { RulewizardComponent } from './rulewizard.component';

@NgModule({
  imports: [
    CommonModule,
    RulewizardRoutingModule
    
  ],
  declarations: []
})
export class RulewizardModule { }
