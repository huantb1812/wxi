import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AppChildrenComponent } from './components/app-children/app-children.component';
import { AppService } from "../../app.service";
 
@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule
  ],
  declarations: [DashboardComponent, AppChildrenComponent],
  providers:[AppService]
})
export class DashboardModule { }
