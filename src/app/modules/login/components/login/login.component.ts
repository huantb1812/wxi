import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AppService } from "../../../../app.service";
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private user: any;
  public name: string;
  public pass: string;
  constructor(private router: Router, private appService: AppService) {
    this.name = '';
    this.pass = '';
  }

  ngOnInit() {
    this.appService.user.isAuthor = false;

  }

  login() {
    if (true) {
      this.appService.user.isAuthor = true;
      localStorage.setItem("isAuthor", 'true');
      this.router.navigate(["/dashboard"]);
    } else {
      // alert('error: user name or password not match');
    }
  }
}
